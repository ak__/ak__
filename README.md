# 👋 Hello! I'm Andrew and I like to build and secure software.

## **Senior Application Security Engineer**, [GitLab](https://gitlab.com/) (Remote)
**July 2019 - Present**
### [Core Responsibilities](https://about.gitlab.com/job-families/engineering/application-security/)
* Conduct security reviews through code review and dynamic testing
* Vulnerability and dependency management
* [Bug bounty program](https://hackerone.com/gitlab) management and report triage
* [Threat modeling](https://about.gitlab.com/handbook/security/threat_modeling/) and [feature-focused security reviews](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/appsec-reviews.html)
* Application Security [stable counterpart](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/stable-counterparts.html) to support and advise [GitLab's Growth, Fulfillment, and Enablement stages](https://about.gitlab.com/handbook/product/categories/)
* Build and extend in-house tooling to automate Application Security tasks including handling and [certifying security releases](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/release-certification.html#certification-process)

### [Acting](https://about.gitlab.com/handbook/people-group/promotions-transfers/#acting) Manager, [Security Research](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/security-research/)
**September 2021 - January 2022**
* Work with team members to plan [Objectives and Key Results](https://about.gitlab.com/company/okrs/) and communicate progress to security leadership
* Hold weekly [1-on-1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with individual contributors
* Lead security engineering incident response as needed
* Attend and participate in security leadership meetings

### Other Activities
* Member of the [Security Culture Committee](https://about.gitlab.com/handbook/engineering/security/security-culture.html) FY22
* Security Engineering instructor for the [Morehouse College of Advanced Software Engineering Course](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/advanced-software-engineering-course/) 2021
* Design and implement [security review](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/jihu-contribution-review-process.html) and [release certification process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/release-certification.html#certification-process) to support GitLab contributions from [JiHu](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/)
* Conduct technical interviews of candidates for Application Security and other security department roles
* Voluntary [Security Incident Response Team](https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/) [on-call rotations](https://about.gitlab.com/handbook/engineering/security/secops-oncall.html) during team ramp-up


## **Senior Software Engineer**, [Hireology](https://hireology.com/) (Remote)
**January 2015 - July 2019**
### Application Development
* Platform team: stabilizing legacy Rails and React code, transitioning to Golang services
* Integrations team: built and maintained third-party integrations using Rails, ES6, React
* Collaboration and planning with a 100% remote team of engineers, product and UX
* Mentor junior engineers, train QA teammates in technical skills, conduct interviews

### Information Security
* Plan and perform regular application security audits and biannual onsite security audits
* Vulnerability management and mitigation planning, risk analysis, phishing awareness
* Implement information security policies and procedures including vulnerability remediation process and severity rating system, security checklists
* Lead workshops introducing application security fundamentals including endpoint security, identifying cross-site scripting vulnerabilities, and security-minded QA testing

### Systems and Site Reliability
* Acting backup Site Reliability Engineer after completing six month internal mentorship program with Senior SRE focusing on AWS infrastructure, deployment automation, Linux
* Contributed to and maintained deployment automation and developer environment setup using Ansible, implemented endpoint monitoring and alerting using Sensu
* RHEL and CentOS Linux System configuration and administration including identity and access management, patch management, third party library and service upgrades

## **Software Craftsman**, [8th Light](https://8thlight.com/) (Chicago, IL)
**January 2013 - January 2015**
### Software Engineering consultant for numerous projects
* Insurance Platform: single-page JavaScript application for enrolling and interacting with insurance claims using CoffeeScript, Backbone.js, backed with C#, MVC4
* High-traffic social coupon website: transitioned and supported legacy systems on backend team serving critical data using Rails
* Mental health patient check-in and profile service using Rails and CoffeeScript

### Training Services
* Planned and taught a multi-day CoffeeScript course for client developers
* Conducted three day workshop on Unix systems fundamentals
* Organized and oversaw a Backbone.js-focused hackathon for client platform

### Mentorship
Worked with three apprentices learning design patterns, test driven development

## Education
B.A. History, [Whitman College](https://www.whitman.edu/), Walla Walla, WA
